const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addCourse = (data) => {

	if(data.isAdmin){

		// Create a variable "newCourse" and instantiates a new "Course" object
		let newCourse = new Course({
			name: data.course.name,
			description : data.course.description,
			price : data.course.price
		})

		// Save after instantiate
		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
			return true;
			}
		})
	}
	else{
		return Promise.resolve(false);
	};

};

// Controller function for getting all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Controler function for getting specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Contoller function for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Controller for archiving a course
module.exports.archiveCourse = (reqParams) => {

		let archivedCourse = {
			isActive : false
		}
	
		// Save after instantiate
		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		});

}