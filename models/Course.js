const mongoose = require("mongoose");

// new mongoose schema

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, "Course is required"]
	},
	description : {
		type: String,
		require: [true, "Description is required"]
	},
	price : {
		type: Number,
		require: [true, "Price is required"]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				require: [true, "UserId is required"]
			},
			enrolledOn : {
				type: Date,
				default: new Date()
			}
		}
	]
})

// Export model
// "Course" = name of model
module.exports = mongoose.model("Course", courseSchema);