// it makes data more secure going from backend to frontend
const jwt = require("jsonwebtoken");

// User defined string data that will be used to create JSON web tokens
const secret = "CourseBookingAPI"

// Token creation
// createAccessToken is user defined
module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	// data & secret are parameters
	return jwt.sign(data, secret, {});
}

// Token verification
module.exports.verify = (req, res, next) => {
	// Teh token is retrieved from the request header
	let token = req.headers.authorization;

	// Token is received and is not defined
	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);

		// validate the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			// if JWT is not valid
			if (err){
				return res.send({auth : "failed"})
			}
			// if JWT is valid
			else{
				// This will allow the application to proceed for the next middleware function/callback function in the route
				next();
			}
		})
	}
	// Token does not exist
	else{
		return res.send({auth : "failed"});
	}
}

/*
	sampFunction(callBackFunciton => {
	
	})

*/
// Token decryption
module.exports.decode = (token) => {
	// Token received and is not defined
	if(typeof token !== "undefined"){
		// Retrieves only the token and removes the "Bearer" prefix
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null;
			}
			else{
				// The "decode" method is used to obtain the information from JWT
				// The ({complete :true}) option allows us to return additional information from the JWT
				// The payload property contains the information provided in the "createAccessToken" method
				return jwt.decode(token, {complete: true}).payload
				}
			}
		)
	}
	// Token does not exist
	else{
		return null;
	}
}